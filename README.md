# React Demo

Prerequisites

* node
* webpack

### Setup ###

Follow these steps to setup your new repository with the boilerplate

1. `cd app`

2. `npm install`

3. `npm start`

4. Open browser to `localhost:8080`