import React, { Component } from 'react'
import data from './sessions.json'

import './App.scss'


export default class Header extends Component{

  /*
  Construstor to set state
   */
  constructor(props){
    super(props)
    this.state = {
      tracks: [],
      sessions: [],
      currentSession: null,
      showMenu: false,
      showSession: false
    }

    //Binding handlers to this to preserve 'this'
    this.handleClick = this.handleClick.bind(this)
    this.showMenu = this.showMenu.bind(this)
  }

  //Build data before components are set
  componentWillMount() {
    this.setState({
      tracks: this.buildTracks(data.Items),
      sessions: this.buildSessions(data.Items)
    })
  }

  //Build Tracks navigation
  buildTracks(items) {
    return items.reduce((cur, next, index) => {
      
      if(!cur.includes(next.Track.Title))
        cur.push(next.Track.Title)
      
      return cur
    },[])
  }

  //Build Session for right rail based on track, default will be the Build track
  buildSessions(sessions, track = "Build") {
    return sessions.filter((session) => {
      return session.Track.Title == track
    })
  }

  //Get Session by Id
  getSession(sessionId){
    this.setState({
      currentSession: this.state.sessions.find((session) => {
        return session.Id == sessionId
      }),
      showSession: true
    }) 
  }

  //Handle Track click to show track based on what is clicked
  handleClick(track) {
    this.setState({
      sessions: this.buildSessions(data.Items, track),
      currentSession: null,
      showMenu: false
    })
  }

  // Show mobile menu
  showMenu(){
    this.setState({
      showMenu: !this.state.showMenu
    })
  }

  /*
  Building the app based on data that has been provided. Some of these can be split into components but for timing kept in on file
   */
  render(){

    let navList = this.state.tracks.map((navItem) => {
      return <li key={navItem} onClick={() => this.handleClick(navItem)}>{navItem}</li>
    })
    
    let sessions = this.state.sessions.map((session) => {
      let sessionSpeakers = ''
      if(session.hasOwnProperty('Speakers')){
        sessionSpeakers = session.Speakers.map((speaker) => {
              return <p key={speaker.AttendeeID}>{`${speaker.FirstName} ${speaker.LastName}, ${speaker.Company}`}</p>
            })
      }
      return <li key={session.Id} onClick={() => this.getSession(session.Id)}>
        <h3>{session.Title}</h3>
        {sessionSpeakers}
      </li>
    })
    
    let session = (this.state.currentSession == null) ? this.state.sessions[0] : this.state.currentSession
    
    let speakers = ''
    if(session.hasOwnProperty('Speakers')){
      speakers = session.Speakers.map((speaker) => {
        return <span className="speaker" key={speaker.AttendeeID}>{`${speaker.FirstName} ${speaker.LastName}, ${speaker.Company}`}</span>
      })
    }

    let speakersBio = ''
    if(session.hasOwnProperty('Speakers')){
      speakersBio = session.Speakers.map((speaker) => {
        return <p key={speaker.AttendeeID}>
          <span className="speaker" key={speaker.AttendeeID}>
            <strong>{`${speaker.FirstName} ${speaker.LastName}`}</strong>, {`${speaker.Title}, ${speaker.Company}`}
          </span>
          {(speaker.Biography != "" ) ? speaker.Biography : 'No bio information' }
          </p>
      })
    }

    return(
      <div>
        <header>
          <img src={require('./logo.svg')} />
          <div className="menuItem" onClick={this.showMenu}>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </header>
        <div className="container">
          <img src={require('./summit-logo.png')} />
          <nav className={`${(this.state.showMenu) ? 'active' : ''}`}>
            <ul>
              {navList}
            </ul>
          </nav>
          <section className="session-list">
            <ul>
              {sessions}
            </ul>
          </section>
          <section className={`session-description ${this.state.showSession ? 'active' : ''}`}>
            <span className="close" onClick={()=> this.setState({showSession: false})}>x</span>
            <h2>{session.Title}</h2>
            {speakers}
            <p>{session.Description}</p>
            <div className="about-speakers">
              <h3>About the speaker</h3>
              {speakersBio}
            </div>
          </section>
        </div>
      </div>
    )
  }
}